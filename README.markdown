# Fork of Slim 2.6.4

This is a fork of the original [slim](https://github.com/slimphp/Slim) by Josh Lockhart.


## Modifications

- Fix warnings and errors related to class inheritance on PHP8. The changes are on [this commit](https://gitlab.cern.ch/fence/common/glance-slim/-/commit/27f88dfb7b6a9c14551a9fdd78bf05d13b6cfc91).

## License

This software is licensed under the MIT License. See the LICENSE file for more information.

